package nau.skytube.gui.businessobjects;


import nau.skytube.businessobjects.YouTube.POJOs.YouTubePlaylist;

/**
 * An interface to return a YouTube Playlist
 */
public interface YouTubePlaylistListener {
	void onYouTubePlaylist(YouTubePlaylist playlist);

}
