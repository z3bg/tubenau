package nau.skytube.mynau;

import org.json.JSONException;
import org.json.JSONObject;

public interface AsyncResponseUpdate {
    void processFinishUpdate(JSONObject output, int resultCode) throws JSONException;
}