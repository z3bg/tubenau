package nau.skytube.mynau;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import nau.skytube.R;
import org.json.JSONException;
import org.json.JSONObject;

public class UserSettingsActivity extends AppCompatActivity implements AsyncResponse, AsyncResponseUpdate {
    String token;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Intent intent = getIntent();
        try {
            token = intent.getStringExtra("token");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Api.getUserDashboard userDashboard = new Api.getUserDashboard();
        userDashboard.delegate = this;
        userDashboard.execute(token);

        ImageButton back = findViewById(R.id.settings_back);
        back.setOnClickListener(v -> finish());

        Button logout = findViewById(R.id.settings_logout);
        logout.setOnClickListener(v -> logoutUser());

        Button delete = findViewById(R.id.settings_delete);
        delete.setOnClickListener(v -> {
            Api.sendDeleteRequest userDelete = new Api.sendDeleteRequest();
            userDelete.delegate = this;
            userDelete.execute(token);
        });
    }

    @Override
    public void processFinish(JSONObject output, int resultCode) throws JSONException {
        String user_photo;
        user_photo = output.getString("avatar");

        new DownloadImageTask(findViewById(R.id.user_settings_pic)).execute(user_photo);

        TextView coinsV = findViewById(R.id.coins_value);
        Integer balance = output.getInt("balance");
        coinsV.setText(balance.toString());

        String name = output.getString("name");
        TextView userName = findViewById(R.id.settings_user_name);
        userName.setText(name);
    }

    public void logoutUser() {
        editor.remove(getString(R.string.token)).apply();
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail().build();
        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            mGoogleSignInClient.signOut();
        }
        finish();
    }

    @Override
    public void processFinishUpdate(JSONObject output, int resultCode) {
        logoutUser();
    }
}

