package nau.skytube.mynau;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import nau.skytube.R;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements AsyncResponse {

    ProgressBar mProgressView;
    EditText email;
    EditText password;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        VideoView videoview = findViewById(R.id.background);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.background);
        videoview.setVideoURI(uri);
        videoview.setOnPreparedListener(mp -> mp.setLooping(true));
        videoview.start();

        // Set up the login form.
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        Button SignInButton = findViewById(R.id.email_sign_in_button);
        SignInButton.setOnClickListener(view -> attemptLogin(email.getText().toString(), password.getText().toString()));

        FloatingActionButton registryBtn = findViewById(R.id.registryBtn);
        registryBtn.setOnClickListener(view -> {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), RegistryActivity.class);
            startActivity(intent);
        });

        mProgressView = findViewById(R.id.login_progress);
        result = findViewById(R.id.result);
    }

    /*
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin(String email, String password) {
        if (isEmailValid(email) && isPasswordValid(password)) {
            mProgressView.setVisibility(View.VISIBLE);
            Api.doLoginTask login = new Api.doLoginTask();
            login.delegate = this;
            login.execute(email, password);
        }
    }

    @Override
    public void processFinish(JSONObject output, int resultCode) {
        mProgressView.setVisibility(View.GONE);
        try {
            if (output.getBoolean("success") == true) {
                JSONObject data = (JSONObject) output.get("data");
                String token = data.getString("token");
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), UserPanelActivity.class);
                intent.putExtra("token", token);
                startActivity(intent);
                finish();
            } else {
                String message = output.getString("error");
                result.setText(message);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.setText("An error ocurred, pleae try again in a few seconds... if the problem persist please contact us through our website nau.mobi");
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }
}

