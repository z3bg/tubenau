package nau.skytube.mynau;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.adgatemedia.sdk.classes.AdGateMedia;
import com.adgatemedia.sdk.network.OnOfferWallLoadFailed;
import com.adgatemedia.sdk.network.OnOfferWallLoadSuccess;

import nau.skytube.BuildConfig;
import nau.skytube.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class UserPanelActivity extends AppCompatActivity implements AsyncResponse {

    String token;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_panel);

        String appHash = "rgkrsxlijqp.7436892987";

        sharedPreferences = getSharedPreferences("terms", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        Intent intent = getIntent();
        try {
            token = intent.getStringExtra("token");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Api.getUserDashboard userDashboard = new Api.getUserDashboard();
        userDashboard.delegate = this;
        userDashboard.execute(token);

        if (getFirstOpen() == null) {
            setFirstOpen();
            Intent intent2 = new Intent();
            intent2.setClass(getApplicationContext(), Terms.class);
            startActivity(intent2);
        }

        ImageButton back = findViewById(R.id.back);
        back.setOnClickListener(v -> finish());

        Button myProfile = findViewById(R.id.my_profile);
        myProfile.setOnClickListener(v -> {
            Intent intent3 = new Intent();
            intent3.setClass(getApplicationContext(), UserProfileActivity.class);
            intent3.putExtra("user", user);
            startActivity(intent3);
        });

        ImageButton settings_btn = findViewById(R.id.settings_btn);
        settings_btn.setOnClickListener(v -> {
            Intent intent4 = new Intent();
            intent4.setClass(getApplicationContext(), UserSettingsActivity.class);
            intent4.putExtra("token", user.token);
            startActivity(intent4);
        });

        Button buyPrizes = findViewById(R.id.buy_prizes);
        buyPrizes.setOnClickListener(v -> {
            Intent intent5 = new Intent();
            intent5.setClass(getApplicationContext(), UserPrizesActivity.class);
            intent5.putExtra("user", user);
            startActivity(intent5);
        });

        Button inviteFriends = findViewById(R.id.invite_friends);
        inviteFriends.setOnClickListener(v -> {

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Browser NAU");
            i.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=nau.browser.free&hl=pt_PT");
            startActivity(Intent.createChooser(i, "Share"));

        });

        Button superrewards = findViewById(R.id.superrewards);
        superrewards.setOnClickListener(v -> {

            //Intent intent = new Intent();
            //intent.setClass(getApplicationContext(), EarnCoinsActivity.class);
            //intent.putExtra("token", user.token);
            //startActivity(intent3);

            final HashMap<String, String> subids = new HashMap<String, String>();
            subids.put("s2", user.name);
            AdGateMedia adGateMedia = AdGateMedia.getInstance();
            adGateMedia.setDebugMode(BuildConfig.DEBUG);

            adGateMedia.loadOfferWall(UserPanelActivity.this,
                    "na2YrA",
                    user.id.toString(),
                    subids,
                    new OnOfferWallLoadSuccess() {
                        @Override
                        public void onOfferWallLoadSuccess() {
                            // Here you can call adGateMedia.showOfferWall();

                            AdGateMedia.getInstance().showOfferWall(UserPanelActivity.this,
                                    new AdGateMedia.OnOfferWallClosed() {
                                        @Override
                                        public void onOfferWallClosed() {
                                            // Here you handle the 'Offer wall has just been closed' event
                                        }
                                    });
                        }
                    },
                    new OnOfferWallLoadFailed() {
                        @Override
                        public void onOfferWallLoadFailed(String reason) {
                            // Here you handle the errors with provided reason
                        }
                    });

        });
    }

    private String getFirstOpen() {
        return sharedPreferences.getString("terms", null);
    }

    private void setFirstOpen() {
        editor.putString("terms", "OK").commit();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void processFinish(JSONObject output, int resultCode) {
        if (output != null) {
            if (output.has("id")) {
                try {
                    String user_photo;
                    user_photo = output.getString("avatar");
                    Log.e("Photo: ", user_photo);

                    new DownloadImageTask(findViewById(R.id.profile_pic)).execute(user_photo);

                    TextView coinsV = findViewById(R.id.coins_value);
                    Integer balance = output.getInt("balance");
                    coinsV.setText(balance.toString());

                    String name = output.getString("name");
                    TextView userName = findViewById(R.id.username);
                    userName.setText(name);

                    user = new User(name, token);

                    user.id = output.getInt("id");
                    // user.slug = output.getString("slug");
                    user.avatar = user_photo;
                    user.balance = balance;

                    JSONObject profile = output.getJSONObject("profile");
                    JSONObject country;
                    if (profile != null) {
                        country = profile.getJSONObject("country");
                        user.country = country.getInt("id");
                        user.country_name = country.getString("name");
                        user.birthdate = profile.getString("birthdate");
                        user.sex = profile.getString("sex");
                    }
                    //Log.v("myNau", "User from API: " + user.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                String message = output.toString();
                // result.setText(message);
            }
        } else {
            Toast.makeText(this, "Error connecting to server.", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences = getSharedPreferences(getString(R.string.token), Context.MODE_PRIVATE);
        String token = sharedPreferences.getString(getString(R.string.token), null);
        // Log.v("myNau", token);
        if (token == null) {
            finish();
        } else {
            Api.getUserDashboard userDashboard = new Api.getUserDashboard();
            userDashboard.delegate = this;
            userDashboard.execute(token);
        }
    }
}
