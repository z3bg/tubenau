package nau.skytube.mynau;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import nau.skytube.R;
import org.json.JSONException;
import org.json.JSONObject;

public class RegistryActivity extends AppCompatActivity implements AsyncResponse {

    ProgressBar mProgressView;
    TextView result;
    EditText name, email, password;
    Button SignUpButton, BackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registry);

        VideoView videoview = findViewById(R.id.background);
        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.background);
        videoview.setVideoURI(uri);
        videoview.setOnPreparedListener(mp -> mp.setLooping(true));
        videoview.start();

        name = findViewById(R.id.reg_name);
        email = findViewById(R.id.reg_email);
        password = findViewById(R.id.reg_password);

        mProgressView = findViewById(R.id.login_progress);
        result = findViewById(R.id.result);

        SignUpButton = findViewById(R.id.email_sign_up_button);
        SignUpButton.setOnClickListener(view -> {
            result.setText("");
            attemptRegistry(name.getText().toString(), email.getText().toString(), password.getText().toString());
        });

        BackButton = findViewById(R.id.email_back_button);
        BackButton.setOnClickListener(view -> finish());
    }

    private void attemptRegistry(String name, String email, String password) {
        if (isEmailValid(email) && isPasswordValid(password) && isNameValid(name)) {
            mProgressView.setVisibility(View.VISIBLE);
            Api.doRegistryTask registry = new Api.doRegistryTask();
            registry.delegate = this;
            registry.execute(name, email, password);

        } else if (!isEmailValid(email)){
            result.setText("Please verify your e-mail.");
        } else if (!isPasswordValid(email)){
            result.setText("Your password must have at least 4 characters.");
        } else if (!isNameValid(email)){
            result.setText("Your name must have at least 3 characters.");
        } else {
            result.setText("A strange error occured!");
        }
    }

    @Override
    public void processFinish(JSONObject output, int resultCode) {
        mProgressView.setVisibility(View.GONE);
        try {
            String message = output.get("message").toString();
            result.setText(message);
            if (output.getBoolean("success") == true) {
                SignUpButton.setVisibility(View.GONE);
                BackButton.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            result.setText("An error ocurred, pleae try again in a few seconds... if the problem persist please contact us through our website nau.mobi");
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private boolean isNameValid(String name) {
        //TODO: Replace this with your own logic
        return name.length() > 3;
    }
}
