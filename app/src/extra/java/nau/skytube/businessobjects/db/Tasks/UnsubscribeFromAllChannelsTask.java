package nau.skytube.businessobjects.db.Tasks;

import android.os.Handler;

import java.io.IOException;
import java.util.List;

import nau.skytube.app.SkyTubeApp;
import nau.skytube.businessobjects.AsyncTaskParallel;
import nau.skytube.businessobjects.Logger;
import nau.skytube.businessobjects.YouTube.POJOs.YouTubeChannel;
import nau.skytube.businessobjects.db.SubscriptionsDb;
import nau.skytube.gui.businessobjects.adapters.SubsAdapter;

/**
 * An Asynctask class that unsubscribes user from all the channels at once.
 */
public class UnsubscribeFromAllChannelsTask extends AsyncTaskParallel<YouTubeChannel, Void, Void> {

	private Handler handler = new Handler();

	@Override
	protected Void doInBackground(YouTubeChannel... youTubeChannels) {
		try {
			List<YouTubeChannel> channelList = SubscriptionsDb.getSubscriptionsDb().getSubscribedChannels();

			for (final YouTubeChannel youTubeChannel : channelList) {
				SubscriptionsDb.getSubscriptionsDb().unsubscribe(youTubeChannel);

				handler.post(new Runnable() {
					@Override
					public void run() {
						SubsAdapter.get(SkyTubeApp.getContext()).removeChannel(youTubeChannel);
					}
				});
			}
		} catch (IOException e) {
			Logger.e(this, "Error while unsubscribing from all channels", e);
		}

		return null;
	}

}
