package nau.skytube.businessobjects.interfaces;

import nau.skytube.gui.fragments.YouTubePlayerV1Fragment;
import nau.skytube.gui.fragments.YouTubePlayerV2Fragment;

/**
 * Interface used by {@link YouTubePlayerV1Fragment} & {@link YouTubePlayerV2Fragment}
 */
public interface YouTubePlayerFragmentInterface {
	void videoPlaybackStopped();
}
